#include <fstream>
#include "GN10/GNInit.hpp"
#include "GN10/GNLogger.hpp"
using namespace GN10;
int main() {
    GNLogger MAIN_CPP_LOGGER = GNLogger("Main");
    MAIN_CPP_LOGGER.info("Gaia Nullius version 1.0 initializing...");
    //TODO width and height should be moved to config, instead of being directly initialized.
    GNInit::init();
}