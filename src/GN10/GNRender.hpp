#ifndef GAIA_NULLIUS_GNRENDER_HPP
#define GAIA_NULLIUS_GNRENDER_HPP

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <vector>

#include "GNAssetLoader.hpp"
namespace GN10 {
    class GNRender {
    public:
        static void renderInit();
    private:
        static GLFWwindow* window;
        static std::vector<GLfloat[]> objects;
        static void collectObjects();

        void renderCycle(GLFWwindow *window);

        void draw(GLFWwindow *window);
    };
}

#endif //GAIA_NULLIUS_GNRENDER_HPP
