//
// Created by mae on 2/5/21.
//

#ifndef GAIA_NULLIUS_GNLOGGER_HPP
#define GAIA_NULLIUS_GNLOGGER_HPP

#include <stdio.h>
#include <string>
#include <ctime>

namespace GN10 {
    class GNLogger {
    private:
        std::string className;
    public:
        explicit GNLogger(const std::string& className) {this->className = className;};
        void debug(const std::string& message);
        void info(const std::string& message);
        void warn(const std::string& message);
        void err(const std::string& message);
        void fatal(const std::string& message);
    };
}

#endif //GAIA_NULLIUS_GNLOGGER_HPP
