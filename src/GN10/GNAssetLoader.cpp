//
// Created by mae on 2/5/21.
//

#define STB_IMAGE_IMPLEMENTATION

#include <fstream>
#include <vector>
#include "stb_image.h"

#include "GNAssetLoader.hpp"
using namespace GN10;

const GLFWimage* GNAssetLoader::loadImage(const std::string& path, int width, int height) {
    return new GLFWimage{width, height, stbi_load(((std::string)std::getenv("GN_ROOT") + "resources/" + path).c_str(), &width, &height, nullptr, 4)};
}
std::vector<std::string> GNAssetLoader::readText(const std::string& path) {
    std::string line;
    std::vector<std::string> contents;
    std::ifstream file((std::string)std::getenv("GN_ROOT") + "resources/" + path);
    if(file.is_open()) {
        while(getline(file, line))
            contents.push_back(line + "\n");
        file.close();
    } else throw std::runtime_error("Failed to open file " + path);
    return contents;
}
