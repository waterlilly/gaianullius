#include "GNLogger.hpp"
using namespace GN10;

void GNLogger::debug(const std::string& message) {
    time_t current_time = time(nullptr);
    tm *local_time = localtime(&current_time);
    std::string hour = std::to_string(local_time->tm_hour);
    hour = hour.length() > 1 ? hour : '0' + hour;
    std::string min = std::to_string(local_time->tm_min);
    min = min.length() > 1 ? min : '0' + min;
    std::string sec = std::to_string(local_time->tm_sec);
    sec = sec.length() > 1 ? sec : '0' + sec;
    char *time_format = new char[9];
    std::snprintf(time_format, 9, "%s:%s:%s", hour.c_str(), min.c_str(), sec.c_str());
    std::fprintf(stdout, "[%s] [%s/DEBUG] %s\n", time_format, this->className.c_str(), message.c_str());
}

void GNLogger::info(const std::string& message) {
    time_t current_time = time(nullptr);
    tm *local_time = localtime(&current_time);
    std::string hour = std::to_string(local_time->tm_hour);
    hour = hour.length() > 1 ? hour : '0' + hour;
    std::string min = std::to_string(local_time->tm_min);
    min = min.length() > 1 ? min : '0' + min;
    std::string sec = std::to_string(local_time->tm_sec);
    sec = sec.length() > 1 ? sec : '0' + sec;
    char *time_format = new char[9];
    std::snprintf(time_format, 9, "%s:%s:%s", hour.c_str(), min.c_str(), sec.c_str());
    std::fprintf(stdout, "[%s] [%s/INFO] %s\n", time_format, this->className.c_str(), message.c_str());
}

void GNLogger::warn(const std::string& message) {
    time_t current_time = time(nullptr);
    tm *local_time = localtime(&current_time);
    std::string hour = std::to_string(local_time->tm_hour);
    hour = hour.length() > 1 ? hour : '0' + hour;
    std::string min = std::to_string(local_time->tm_min);
    min = min.length() > 1 ? min : '0' + min;
    std::string sec = std::to_string(local_time->tm_sec);
    sec = sec.length() > 1 ? sec : '0' + sec;
    char *time_format = new char[9];
    std::snprintf(time_format, 9, "%s:%s:%s", hour.c_str(), min.c_str(), sec.c_str());
    std::fprintf(stdout, "[%s] [%s/WARN] %s\n", time_format, this->className.c_str(), message.c_str());
}

void GNLogger::err(const std::string& message) {
    time_t current_time = time(nullptr);
    tm *local_time = localtime(&current_time);
    std::string hour = std::to_string(local_time->tm_hour);
    hour = hour.length() > 1 ? hour : '0' + hour;
    std::string min = std::to_string(local_time->tm_min);
    min = min.length() > 1 ? min : '0' + min;
    std::string sec = std::to_string(local_time->tm_sec);
    sec = sec.length() > 1 ? sec : '0' + sec;
    char *time_format = new char[9];
    std::snprintf(time_format, 9, "%s:%s:%s", hour.c_str(), min.c_str(), sec.c_str());
    std::fprintf(stderr, "[%s] [%s/ERR] %s\n", time_format, this->className.c_str(), message.c_str());
}

void GNLogger::fatal(const std::string& message) {
    time_t current_time = time(nullptr);
    tm *local_time = localtime(&current_time);
    std::string hour = std::to_string(local_time->tm_hour);
    hour = hour.length() > 1 ? hour : '0' + hour;
    std::string min = std::to_string(local_time->tm_min);
    min = min.length() > 1 ? min : '0' + min;
    std::string sec = std::to_string(local_time->tm_sec);
    sec = sec.length() > 1 ? sec : '0' + sec;
    char *time_format = new char[9];
    std::snprintf(time_format, 9, "%s:%s:%s", hour.c_str(), min.c_str(), sec.c_str());
    std::fprintf(stderr, "[%s] [%s/FATAL] %s\n", time_format, this->className.c_str(), message.c_str());
}
