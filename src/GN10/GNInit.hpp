#ifndef GAIA_NULLIUS_GNINIT_HPP
#define GAIA_NULLIUS_GNINIT_HPP

#include "GNRender.hpp"

#include <cstdlib>
#include <string>
#include <iostream>


namespace GN10 {
    class GNInit {
    public:
        static void init(); //Initializes the GLFW window and OpenGL context
    };
}

#endif //GAIA_NULLIUS_GNINIT_HPP
