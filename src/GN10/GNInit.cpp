#include <fstream>
#include "GNInit.hpp"
#include "GNLogger.hpp"

using namespace GN10;

void GNInit::init() {
    //Initialize logger
    GNLogger GN_INIT_LOGGER = GNLogger("GNInit");

    //Test if file reading is working by reading resources/test.txt
    GN_INIT_LOGGER.info("Testing for file read capabilities...");
    GN_INIT_LOGGER.debug("GN_ROOT = " + (std::string)std::getenv("GN_ROOT"));
    //This vector will contain the contents of the file, the string contains that vector, converted into a single string for comparison
    std::vector<std::string> test_file_contents = GNAssetLoader::readText("test.txt");
    std::string contents;
    //Iterate through vector, appending the line onto the end of the string
    for(auto &lines : test_file_contents) {
        contents.append(lines);
    }
    //This is the comparison
    if(contents == "test\ntest 2\n")
        GN_INIT_LOGGER.info("Test of file read capabilities successful!");
    else {
        GN_INIT_LOGGER.debug("test.txt read as \"" + contents + "\"");
        GN_INIT_LOGGER.fatal("Failed to read test file! GN_ROOT may be set incorrectly, or the filesystem may not be readable.");
        throw std::runtime_error("GNInit: Failed to read test file");
    }
    //This line initializes GLFW

    GNRender::renderInit();
}