//
// Created by mae on 2/5/21.
//

#ifndef GAIA_NULLIUS_GNASSETLOADER_HPP
#define GAIA_NULLIUS_GNASSETLOADER_HPP

#include <GLFW/glfw3.h>
#include <string>

namespace GN10 {
    class GNAssetLoader {
    public:
        static const GLFWimage * loadImage(const std::string& path, int width, int height);
        static std::vector<std::string> readText(const std::string &path);
    };
}

#endif //GAIA_NULLIUS_GNASSETLOADER_HPP
