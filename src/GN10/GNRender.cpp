#include "GNRender.hpp"
#include "GNLogger.hpp"

using namespace GN10;
void GNRender::renderInit() {
    GNLogger GN_RENDER_LOGGER = GNLogger("GNRender");
    if(!glfwInit()) {
        GN_RENDER_LOGGER.fatal("Failed to initialize GLFW!");
        glfwTerminate();
    }
    //Sets up some runtime settings. TODO read these from a config file/from the system configuration
    glfwWindowHint(GLFW_SAMPLES, 2); // 2x MSAA
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5); // OpenGL Version 4.5 in use
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // Forward compatible core profile
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); //Use core profile instead of compatibility profile
    glfwWindowHint(GLFW_REFRESH_RATE, 60); //60 FPS
    //Open the window.
    GN_RENDER_LOGGER.info("Setting up window...");
    GNRender::window = glfwCreateWindow(1280, 720, "Gaia Nullius 1.0", nullptr, nullptr);
    if(!GNRender::window) { //Make sure window initialized correctly
        GN_RENDER_LOGGER.fatal("Failed to initialize window!");
        glfwTerminate();
    }
    //Activate OpenGL context
    glfwMakeContextCurrent(GNRender::window);
    GN_RENDER_LOGGER.info("Successfully initialized window!");
    //Initialize GLEW
    glewExperimental = GL_TRUE;
    if (glewInit() != GLEW_OK) {
        GN_RENDER_LOGGER.fatal("Failed to initialize GLEW!");
        glfwTerminate();
    }
    glfwSetWindowIcon(GNRender::window, 1, GNAssetLoader::loadImage("gn-logo.png", 128, 128));
    GN_RENDER_LOGGER.info("Successfully completed initialization! Beginning render loop...");
    //Basic init is done, moving to initialize the loop manager. TODO get the loop manager set up
    while(!glfwWindowShouldClose(window)) {
        renderCycle();
    }
}
void GNRender::renderCycle(GLFWwindow* window) {
    collectObjects();
    draw(window);
}

void GNRender::draw(GLFWwindow* window) {
    glClear(GL_COLOR_BUFFER_BIT);
    glfwSwapBuffers(window);
    glfwPollEvents();
}

void GNRender::collectObjects() {

}